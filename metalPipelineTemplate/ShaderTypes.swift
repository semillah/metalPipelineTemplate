//
//  ShaderTypes.swift
//  metalPipelineTemplate
//
//  Created by Raul on 1/21/24.
//

import Foundation
import simd

struct ScreenDimensions {
    var width: Float
    var height: Float
}

