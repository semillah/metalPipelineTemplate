import Foundation
import MetalKit
import simd



class Renderer: NSObject {
    static var device: MTLDevice!
    static var commandQueue: MTLCommandQueue!
    static var library: MTLLibrary!
    var pipelineState: MTLRenderPipelineState!
    var computePipelineState: MTLComputePipelineState!
    var dimensionsBuffer: MTLBuffer?

    init(metalView: MTKView) {
        super.init()

        guard let device = MTLCreateSystemDefaultDevice(),
              let commandQueue = device.makeCommandQueue() else {
            fatalError("GPU not available")
        }

        Renderer.device = device
        Renderer.commandQueue = commandQueue
        metalView.device = device

        setupShadersAndPipelineState(metalView: metalView)
        setupComputePipelineState()
        updateScreenDimensions(metalView: metalView)
        metalView.delegate = self
    }

    private func setupShadersAndPipelineState(metalView: MTKView) {
        Renderer.library = Renderer.device.makeDefaultLibrary()
        let vertexFunction = Renderer.library?.makeFunction(name: "vertex_main")
        let fragmentFunction = Renderer.library?.makeFunction(name: "fragment_main")

        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = vertexFunction
        pipelineDescriptor.fragmentFunction = fragmentFunction
        pipelineDescriptor.colorAttachments[0].pixelFormat = metalView.colorPixelFormat

        do {
            pipelineState = try Renderer.device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch let error {
            fatalError(error.localizedDescription)
        }
    }

    private func setupComputePipelineState() {
        guard let computeFunction = Renderer.library?.makeFunction(name: "compute_main") else {
            fatalError("Compute function not found")
        }

        do {
            computePipelineState = try Renderer.device.makeComputePipelineState(function: computeFunction)
        } catch let error {
            fatalError("Error creating compute pipeline state: \(error)")
        }
    }

    private func updateScreenDimensions(metalView: MTKView) {
        let size = metalView.bounds.size
        var dimensions = ScreenDimensions(width: Float(size.width), height: Float(size.height))
        dimensionsBuffer = Renderer.device.makeBuffer(bytes: &dimensions, length: MemoryLayout<ScreenDimensions>.size, options: [])
    }

    func performComputePass() {
        guard let commandBuffer = Renderer.commandQueue.makeCommandBuffer(),
              let computeEncoder = commandBuffer.makeComputeCommandEncoder(),
              let dimensionsBuffer = dimensionsBuffer else {
            return
        }

        computeEncoder.setComputePipelineState(computePipelineState)
        computeEncoder.setBuffer(dimensionsBuffer, offset: 0, index: 0) // Using index 0 for the dimensions buffer

        // Dispatch compute commands (adjust parameters based on your compute kernel)
        // Example: computeEncoder.dispatchThreadgroups(threadgroupsPerGrid, threadsPerThreadgroup: threadsPerThreadgroup)

        computeEncoder.endEncoding()
        commandBuffer.commit()
    }
}

extension Renderer: MTKViewDelegate {
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        updateScreenDimensions(metalView: view)
    }

    func draw(in view: MTKView) {
        guard let commandBuffer = Renderer.commandQueue.makeCommandBuffer(),
              let descriptor = view.currentRenderPassDescriptor,
              let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: descriptor),
              let dimensionsBuffer = dimensionsBuffer else {
            return
        }

        renderEncoder.setRenderPipelineState(pipelineState)
        renderEncoder.setVertexBuffer(dimensionsBuffer, offset: 0, index: 0) // Using index 0 for the dimensions buffer
        renderEncoder.endEncoding()

        if let drawable = view.currentDrawable {
            commandBuffer.present(drawable)
        }

        commandBuffer.commit()
    }
}
