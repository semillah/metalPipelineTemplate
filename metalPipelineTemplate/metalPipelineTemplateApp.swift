//
//  metalPipelineTemplateApp.swift
//  metalPipelineTemplate
//
//  Created by Raul on 1/19/24.
//

import SwiftUI

@main
struct metalPipelineTemplateApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
